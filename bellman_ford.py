#!/usr/bin/python
import os

__author__ = 'Francisco Calderon'

import sys
import json

'''
 This script implements the Bellman-Ford Algorithm to find the
 shortest Path in a Graph
'''

help_message = "Usage: [--debug] [--data] data [-h/-help]\n " \
               "--debug: whether to print debug messages (default False)\n" \
               "--data: graph in json format as [{ source: :the_source,  vertices: :number_of_vertices, edges : [[:source_vertex, :destination_vertex, :weight]] \n" \
               "--data_file: the name of a file containing the data" \
               "If data not provided, it will run a demo\n" \
               "example of data format: [ { 'source': 0,  'vertices': 3, 'edges':[[0, 2, 2], [1, 0, -1], [2, 1, 1]]}\n" \
               "The above graph contains three vertices connected to each other. \n" \
               "* Vertex 0 connects to vertex 2 with a weight of 2\n" \
               "* Vertex 1 connects to vertex 0 with a weight of -1\n" \
               "* Vertex 2 connects to vertex 1 with a weight of 1\n"

debug = '--debug' in sys.argv


def debug_message(msg):
    """
    Prints the given message if variable debug is True
    Should be passed as an argument when calling this script
    --debug
    :param msg: the message to print
    :return:
    """
    if debug:
        print(msg)


class Edge(object):

    def __init__(self):
        self.source = 0
        self.destination = 0
        self.weight = 0


class Graph(object):
    def __init__(self, vertices=0, edges=0, edge=None):
        self.vertices = vertices
        self.edges = edges
        self.edge = edge if edge else []


def create_graph(vertices, edges):
    """
    Returns a graph with the given number of vertices and the given number of edges
    :param vertices: the number of vertices the graph will have
    :param edges: the number of edges the graph will have
    :return: A graph object
    """
    debug_message("Creating Graph")
    edge = []

    for i in range(0, edges):
        edge.append(Edge())

    return Graph(vertices, edges, edge)


def json_to_graph(_json):
    e = len(_json["edges"])
    v = _json["vertices"]

    graph = create_graph(v, e)

    counter = 0
    for edge in _json['edges']:
        graph.edge[counter].source = edge[0]
        graph.edge[counter].destination = edge[1]
        graph.edge[counter].weight = edge[2]
        counter += 1
    return graph


def bellman_ford(graph, source, destination=None):
    V = graph.vertices
    E = graph.edges
    distances = []
    path = []
    debug_message("Initializing instances from source (" + str(source) + ") to all other vertices as INF")
    for i in range(0, V):
        distances.append('INF')
    distances[source] = 0

    debug_message("Relaxing all edges " + str(V) + " times")
    for i in range(0, V):
        for j in range(0, E):
            u = graph.edge[j].source
            v = graph.edge[j].destination
            weight = graph.edge[j].weight
            if distances[u] is not 'INF' and distances[u] + weight < distances[v]:
                debug_message("Found path: " + str(u) + " -> " + str(v) + ' w = ' + str(distances[u] + weight))
                distances[v] = distances[u] + weight
                if not destination or v not in path:
                    path.append(v)

    debug_message("Checking for negative-weight cycles")
    for j in range(0, E):
        u = graph.edge[j].source
        v = graph.edge[j].destination
        weight = graph.edge[j].weight

        if distances[u] is not 'INF' and distances[u] + weight < distances[v]:
            print("Negative weight cycle found")
    print_solution(distances, V, path)


def print_solution(_distances, vertices, path=None):
    debug_message("Printing solutions")
    if path:
        s_path = ''
        for i in range(0, len(path)):
            if i + 1 < len(path):
                s_path += str(i) + ' -> '
            else:
                s_path += str(i)
        print "Path: " + s_path

    print repr("VERTEX").rjust(8), repr("DISTANCE FROM SOURCE").rjust(22)
    for i in range(0, vertices):
        print repr(i).rjust(4), repr(_distances[i]).rjust(14)

if __name__ == '__main__':
    if '-h' in sys.argv or '-help' in sys.argv:
        print(help_message)
        exit()
    if debug:
        print("Debug set to True")

    if '--data' not in sys.argv and '--data_file' not in sys.argv:
        if os.path.exists('./demo_data.json'):
            with open('demo_data.json') as data_file:
                data = json.load(data_file)
        else:
            data = json.loads('{"vertices": 5, "source": 0, "edges":[[0, 1, -1], [0, 2, 4], [1, 2, 3], [1, 3, 2], [1, 4, 2], [3, 2, 5], [3, 1, 1], [4, 3, -3]]}')
    elif '--data' in sys.argv:
        raw_data = sys.argv[sys.argv.index('--data') + 1]
        data = json.loads(raw_data)
    else:
        with open(sys.argv[sys.argv.index('--data_file') + 1]) as data_file:
            data = json.load(data_file)

    bellman_ford(json_to_graph(data), data['source'], 5)





